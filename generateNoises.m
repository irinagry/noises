% % % % IRINA GRIGORESCU
% % % % Implementing Different Noises

% clear all
% close all`

addpath helpers/

perm = [ 151,160,137,91,90,15, ...					
    131,13,201,95,96,53,194,233,7,225,140, ...
    36,103,30,69,142,8,99,37,240,21,10,23, ...
    190, 6,148,247,120,234,75,0,26,197,62, ...
    94,252,219,203,117,35,11,32,57,177,33, ...
    88,237,149,56,87,174,20,125,136,171,168, ...
    68,175,74,165,71,134,139,48,27,166, ...
    77,146,158,231,83,111,229,122,60,211, ...
    133,230,220,105,92,41,55,46,245,40,244, ...
    102,143,54, 65,25,63,161, 1,216,80,73,209, ...
    76,132,187,208, 89,18,169,200,196, ...
    135,130,116,188,159,86,164,100,109,198, ...
    173,186, 3,64,52,217,226,250,124,123, ...
    5,202,38,147,118,126,255,82,85,212,207, ...
    206,59,227,47,16,58,17,182,189,28,42, ...
    223,183,170,213,119,248,152, 2,44,154,163, ...
    70,221,153,101,155,167, 43,172,9, ...
    129,22,39,253, 19,98,108,110,79,113,224, ...
    232,178,185, 112,104,218,246,97,228, ...
    251,34,242,193,238,210,144,12,191,179,162, ...
    241, 81,51,145,235,249,14,239,107, ...
    49,192,214, 31,181,199,106,157,184, ...
    84,204,176,115,121,50,45,127, 4,150,254, ...
    138,236,205,93,222,114,67,29,24,72,243, ...
    141,128,195,78,66,215,61,156,180];

TABSIZE = 256;
TABMASK = TABSIZE - 1;
% Index function handle
index = @(idx) perm(idx);


M = 20;
N = 50*M - 49;

% % White Noise
xWhiteNoise = linspace(1,M,N);
whiteNoise  = -1 + 2.*rand(N,1);

% % Filtered White Noise
yFilteredSignalFT = filterSignal(whiteNoise, dx, 0.1, 0.9);
filteredWhiteNoise = real(ifft(yFilteredSignalFT));

% % Value Noise
xValueNoise = linspace(1,M,N);
valueNoise  = zeros(1,N);
% Generate PRNs between -1 and 1
valueTab = -1 + 2 .* rand(TABSIZE,1);
for i = 1:N
    valueNoise(i) = calculateValueNoise(...
        xValueNoise(i), perm, TABMASK, valueTab);
end
% valueNoise = normaliseAB(-1,1,valueNoise);

% % Gradient Noise
gradientTab    = -1 + 2.*rand(TABSIZE,1); %generateUnitVectors(TABSIZE); 
xGradientNoise = linspace(1,M,N);
gradientNoise  = zeros(1,N);
for i = 1:N
    gradientNoise(i) = calculateGradientNoise(...
        xGradientNoise(i), perm, TABMASK, gradientTab);
end
% gradientNoise = normaliseAB(-1,1,gradientNoise);

% % Gradient-Value Noise
xGradientValueNoise = linspace(1,M,N);
gradientValueNoise  = zeros(1,N);
for i = 1:N
    gradientValueNoise(i) = calculateGradientValueNoise(...
        xGradientValueNoise(i), perm, TABMASK, valueTab, gradientTab, 1, 1);
end
% gradientValueNoise = normaliseAB(-1,1,gradientValueNoise);

%%
% % % % % PLOT
noLins = 3;
noCols = 4;
maxF = 1.5;

figure

% White Noise
subplot(noLins,noCols,1)
plot(xWhiteNoise, whiteNoise)
title('White Noise')
grid on
axis([1 M -1 1])
xlabel('x'); ylabel('f(x)');
subplot(noLins,noCols,2)
[xxW, yyW] = prepareForPlotFT(...
                whiteNoise, xWhiteNoise(3)-xWhiteNoise(2), maxF);
stem(xxW, abs(yyW));
xlabel('x^{-1}'); ylabel('|F[f(x)]|');
title({'FFT', 'White Noise'})

% Filtered White Noise
subplot(noLins,noCols,5)
plot(xWhiteNoise, filteredWhiteNoise)
title('Filtered White Noise')
grid on
axis([1 M -1 1])
xlabel('x'); ylabel('f(x)');
subplot(noLins,noCols,6)
[xxFW, yyFW] = prepareForPlotFT(...
                ifft(yFilteredSignalFT), ...
                xWhiteNoise(3)-xWhiteNoise(2), maxF);
stem(xxFW, abs(yyFW));
xlabel('x^{-1}'); ylabel('|F[f(x)]|');
title({'FFT Filtered', 'White Noise'})

% Value Noise
subplot(noLins,noCols,3)
plot(xValueNoise, valueNoise)
grid on
title('Value Noise')
axis([1 M -1 1])
xlabel('x'); ylabel('f(x)');
subplot(noLins,noCols,4)
[xxV, yyV] = prepareForPlotFT(...
                valueNoise, xValueNoise(3)-xValueNoise(2), maxF);
stem(xxV, abs(yyV));
xlabel('x^{-1}'); ylabel('|F[f(x)]|');
title({'FFT', 'Value Noise'})

% Gradient Noise
subplot(noLins,noCols,7)
plot(xGradientNoise, gradientNoise)
grid on
title('Gradient Noise')
axis([1 M -1 1])
xlabel('x'); ylabel('f(x)');
subplot(noLins,noCols,8)
[xxG, yyG] = prepareForPlotFT(...
                gradientNoise, xGradientNoise(3)-xGradientNoise(2), maxF);
stem(xxG, abs(yyG));
xlabel('x^{-1}'); ylabel('|F[f(x)]|');
title({'FFT', 'Gradient Noise'})

% Gradient-Value Noise
subplot(noLins,noCols,11)
plot(xGradientValueNoise, gradientValueNoise)
grid on
title({'Gradient-Value', 'Noise'})
axis([1 M -1 1])
xlabel('x'); ylabel('f(x)');
subplot(noLins,noCols,12)
[xxGV, yyGV] = prepareForPlotFT(...
                gradientValueNoise, ...
                xGradientValueNoise(3)-xGradientValueNoise(2), maxF);
stem(xxGV, abs(yyGV));
xlabel('x^{-1}'); ylabel('|F[f(x)]|');
title({'FFT', 'Gradient-Value', 'Noise'})








