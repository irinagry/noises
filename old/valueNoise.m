function valNoise = valueNoise(N, perm)

TABSIZE = 256;
TABMASK = TABSIZE - 1;

% Index function handle
index = @(ix) perm(ix);

% Generate PRNs between -1 and 1
valueTab = -1 + 2 .* rand(TABSIZE,1);

% Value lattice function handle
vlattice = @(ix) valueTab(index(ix)+1);

% % % % % VALUE NOISE
% Populate lattice points with PRNs
latticeN = ceil(N/4);
latPRNs = zeros(latticeN, 1);
for i = 1:latticeN
    idx = bitand(i-1, TABMASK)+1;
    latPRNs(i) = vlattice(idx);
end

% Interpolate between the PRNs
% valNoise = zeros(2*N, 1);
% for i = 1:N
%     idxVal = (i-1)*2 + 1
%     
%     valNoise(idxVal:(idxVal+3)) = ...
%             spline(0:3, latPRNs(i:(i+3)), 0:3);
%     
% end
% for i = 1:N
%     valNoise(i) = spline(i:(i+3), latPRNs(i:(i+3)), i+1);
% end
xx = linspace(1, latticeN, N);
valNoise = interp1(1:latticeN, latPRNs, xx, 'spline');

valNoise = normaliseAB(-1, 1, valNoise);































% 
% % Populate lattice points with PRNs
% for i = 1:N
%     valueNoise(i) = perm(bitand(i-1, TABMASK)+1);
% end
% % Normalise between -1,1
% valueNoise = normaliseAB(-1, 1, valueNoise);
% 
% % Interpolate between the PRNs
% x = abs(valueNoise(1:N) - [valueNoise(2:N) ; valueNoise(1)]);
% tempNoise = zeros(N,1);
% for i = 1:N
%     if i == N
%         tempNoise(i) = lerp(smoothstep(x(i)), ...
%             valueNoise(i), valueNoise(1));    
%     else
%         tempNoise(i) = lerp(smoothstep(x(i)), ...
%             valueNoise(i), valueNoise(i+1));    
%     end
% end
% valueNoise = tempNoise;
% clear tempNoise
% valueNoise = normaliseAB(-1, 1, valueNoise);
