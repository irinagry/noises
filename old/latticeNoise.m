function latNoise = latticeNoise(N, perm)
% % % % IRINA GRIGORESCU
% % % % Implementing Lattice Noise

TABMASK = 255;

% Linear interpolation function handle
lerp = @(t,x0,x1) (x0 + t.*(x0-x1));
% Smooth step
smoothstep = @(x) x .* x .* (3 - 2.* x);

% Index function handle
index = @(ix) perm(ix);


latPRNs = zeros(N,1);
% Populate lattice points with PRNs
for i = 1:N
    latPRNs(i) = index(bitand(i-1, TABMASK)+1);
end
% latPRNs = normaliseAB(-1, 1, latPRNs);
% 
% % Interpolate between the PRNs
% x = abs(latPRNs(1:N) - [latPRNs(2:N) ; latPRNs(1)]);
% tempNoise = zeros(N,1);
% for i = 1:N
%     if i == N
%         tempNoise(i) = lerp(smoothstep(x(i)), ...
%             latPRNs(i), latPRNs(1));    
%     else
%         tempNoise(i) = lerp(smoothstep(x(i)), ...
%             latPRNs(i), latPRNs(i+1));    
%     end
% end
% latNoise = tempNoise;

% Interpolate between the PRNs
xx = linspace(1, N, 2*N);
latNoise = interp1(1:N, latPRNs, xx);

latNoise = normaliseAB(-1, 1, latNoise);


