function gradNoise = gradientNoise(N, perm)

TABSIZE = 256;
TABMASK = TABSIZE - 1;

% Index function handle
index = @(ix) perm(ix);

% Generate PRNs between -1 and 1
gradTab = -1 + 2 .* rand(TABSIZE,1);

% Value lattice function handle
gradLattice = @(ix) gradTab(index(ix)+1);

% Smooth step
smoothstep = @(x) x .* x .* (3 - 2.* x);

% Linear interpolation function handle
lerp = @(t,x0,x1) (x0 + t.*(x0-x1));

% % % % % VALUE NOISE
% Populate lattice points with gradient PRNs
latPRNs = zeros(N,1);
for i = 1:N
    idx = bitand(i-1, TABMASK)+1;
    latPRNs(i) = gradLattice(idx);
end

latPRNs(latPRNs>0) =  1;
latPRNs(latPRNs<0) = -1;

% Interpolate between the gradient PRNs
xx = linspace(1, N,  4*N);
gradNoise = zeros(1, 4*N);
for i = 1:4*N
    x = xx(i);
    ix = floor(x);
    fx0 = x-ix;
    fx1 = fx0-1;
    wx = smoothstep(fx0);
    vx0 = latPRNs(ix)*fx0;
    vx1 = latPRNs(ix)*fx1;
%     gradNoise((i-1)*2+1) = vx0;
%     gradNoise((i-1)*2+2) = vx1;
%     interpGrads = interp1(1:2, [vx0 vx1], 1:0.5:2, 'spline')
%     vx0
%     vx1
%     interpLinsp = linspace(ix, ix+1, 4);
%     interpGrads = interp1([ix ix+1], [vx0 vx1], interpLinsp, 'spline');
    gradNoise(i) = lerp(wx, vx0, vx1);
end
% xx = linspace(1, N, 8*N);
% gradNoise = interp1(1:4*N, gradNoiseTemp, xx, 'spline');

% gradNoise = normaliseAB(-1, 1, gradNoise);

