function gradValNoise = calculateGradientValueNoise(...
    x, perm, TABMASK, valueTab, gradientTab, weightVal, weightGrad)

% % % % IRINA GRIGORESCU
% % % % 
% % % % This function returns the value of a weighted sum between
% % % % gradient noise and value noise at coordinate x

gradValNoise = (weightVal.*calculateValueNoise(x, perm, TABMASK, valueTab) + ...
                weightGrad.*calculateGradientNoise(x, perm, TABMASK, gradientTab)) ...
                ./(weightVal+weightGrad) ;
