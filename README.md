Generating Noises
============================

Author: Irina Grigorescu 
------------------------------------------------------
Email: <irina.grigorescu.15@ucl.ac.uk>

This work is based on Chapter "Making Noises" from "Texturing and Modeling - A Procedural Approach", 3rd edition, page 67, D. S. Ebert, F. K. Musgrave, D. Peachey, K. Perlin, S. Worley

