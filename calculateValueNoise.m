function valNoise = calculateValueNoise(x, perm, TABMASK, valueTab)
% % % % IRINA GRIGORESCU
% % % % 
% % % % This function returns the value of value Noise at coord x
% % % % based on the table of previously computed PRNs

% Value lattice function handle
vlattice = @(ix) valueTab(perm(bitand(ix-1, TABMASK)+1)+1);

% Fitting a spline through 4 PRNs at positions ix-1, ix, ix+1, ix+2
% and returning the value at coordinate x

% If x < 2 let's just fit the spline through (end,1,2,3)
if x < 2
    ix = floor(x);
    xForInterpolation = [TABMASK ix ix+1 ix+2];
    yForInterpolation(1) = vlattice(xForInterpolation(1));
    yForInterpolation(2) = vlattice(xForInterpolation(2));
    yForInterpolation(3) = vlattice(xForInterpolation(3));
    yForInterpolation(4) = vlattice(xForInterpolation(4));
    
    valNoise = interp1(xForInterpolation, yForInterpolation, x, 'spline');
% If x >= 2 let's fit the spline through (ix-1,ix,ix+1,ix+2)
else
    ix = floor(x);
    xForInterpolation = [ix-1 ix ix+1 ix+2];
    yForInterpolation(1) = vlattice(xForInterpolation(1));
    yForInterpolation(2) = vlattice(xForInterpolation(2));
    yForInterpolation(3) = vlattice(xForInterpolation(3));
    yForInterpolation(4) = vlattice(xForInterpolation(4));
    
    valNoise = interp1(xForInterpolation, yForInterpolation, x, 'spline');
end
    

end