function rez = normaliseAB(a,b,A)
% % % % IRINA GRIGORESCU
% % % % Date created: 03-04-2017
% % % % Date updated: 03-04-2017
% % % % 
% % % % This function normalises the set A to the range [a,b]

% Normalise A
A = (A - min(A));
A = A./max(A);

% Scale it to the bounds [0,(b-a)]
A = (b-a).*A;

% Scale it to [a,b]
rez = A + a;

end