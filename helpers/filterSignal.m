function yFilteredSignal = filterSignal(signalToFilter, ...
                                        dx, minFreq, maxFreq)
% % % % IRINA GRIGORESCU
% % % % 
% % % % INPUT:
% % % %     signalToFilter = (1xN double) the array that will be Fourier
% % % %                     transformed and high freqs will be filtered out
% % % %     dx = (double) sampling interval in the spatial domain
% % % %     
% % % % 
% % % % 

% Number of elements
N = numel(signalToFilter);

% Sampling interval in the reciprocal space domain
df = 1/(N*dx);

% Maximum/Minimum frequency in Fourier domain
fmax = 1/(2*dx);
fmin = 0;

% Maximum/Minimum frequency wanted
if maxFreq < fmax
    fmax = maxFreq;
end
if minFreq > fmin && minFreq < fmax
    fmin = minFreq;
end
    

% Fourier transform the signal
fftSignal = fft(signalToFilter);

% Prepare Signal for plotting
xFilteredSignalMax = 0:df:fmax;
xFilteredSignalMin = 0:df:fmin;
NvaluesMax     = ceil(numel(xFilteredSignalMax));
NvaluesMin     = ceil(numel(xFilteredSignalMin));

yFilteredSignal = fftSignal;

yFilteredSignal(1:NvaluesMin) = 0;
yFilteredSignal(NvaluesMax : end-NvaluesMax) = 0;
yFilteredSignal(end-NvaluesMin:end) = 0;

