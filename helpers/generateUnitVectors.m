function gradientTab = generateUnitVectors(TABSIZE)
% Generate 3D unit vectors

gradientTab = zeros(TABSIZE, 3);

for i = 1:TABSIZE
    z = -1 + 2.*rand(1,1);
    r = sqrt(1 - z.*z);
    theta = 2 .* pi .* rand(1,1);
    
    gradientTab(i,1) = r .* cos(theta);
    gradientTab(i,2) = r .* sin(theta);
    gradientTab(i,3) = z;
end

end