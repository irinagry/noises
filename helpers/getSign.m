function x = getSign(x)
    
    x(x>0) =  1;
    x(x<0) = -1;
    
    x(x==0) = 1;
    
end