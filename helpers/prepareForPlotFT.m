function [xSignalToPlot, ySignalToPlot] = prepareForPlotFT(signalToTransform, dx, maxFreq)
% % % % IRINA GRIGORESCU
% % % % 
% % % % INPUT:
% % % %     signalToTransform = (1xN double) the array that will be Fourier
% % % %                         transformed
% % % %     dx = (double) sampling interval in the spatial domain
% % % %     
% % % % 
% % % % 

% Number of elements
N = numel(signalToTransform);

% Sampling interval in the reciprocal space domain
df = 1./(N*dx);

% Maximum frequency in Fourier domain
fmax = 1./(2*dx);

% Maximum frequency wanted
if maxFreq < fmax
    fmax = maxFreq;
end

% Fourier transform the signal
fftSignal = fft(signalToTransform);

% Prepare Signal for plotting
xSignalToPlot = 0:df:fmax;
ySignalToPlot = fftSignal(1:numel(xSignalToPlot));


